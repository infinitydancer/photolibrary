#!/usr/bin/python3


## OPEN images.csv and print each record

import csv
import mysql.connector

#from pathlib import Path

def read_images():
    file = open('images.csv')
    reader = csv.reader(file)

    for photo_detail in reader:
        path = photo_detail[1]
        date = photo_detail[3]
        photog = photo_detail[4]
        photog_url = photo_detail[5]
        photo_url = photo_detail[6]

        #print('Path:', path, 'Date:', date, 'Photographer:', photog, 'URL to Photographer:', photog_url, 'URL to photo:', photo_url, sep = '  ')
        
        insert_to_sql = ('''INSERT INTO Images (Photo_Path, Photographer, URL_of_Photo, URL_of_Photographer, Date_Created)
        VALUES (%s, %s, %s, %s, %s)''')

        db_cursor.execute(insert_to_sql, (path, photog, photo_url, photog_url, date))

        images_db.commit()


def reCreate_tables():

    drop_Tags = 'DROP TABLE IF EXISTS Tags'
    
    drop_Images = 'DROP TABLE IF EXISTS Images'

    create_Images = '''
    CREATE TABLE if not exists Images(

        ID                  int             NOT NULL    AUTO_INCREMENT,
        Photo_Path          varchar(255)    NOT NULL,
        Photographer        varchar(255),
        URL_of_Photo        varchar(255),
        URL_of_Photographer varchar(255),
        Date_Created        date,

        PRIMARY KEY (ID)
        )'''

    create_Tags = '''
    CREATE TABLE IF NOT EXISTS Tags(
        Image_ID    int         NOT NULL,
        Tag         varchar() NOT NULL,
        PRIMARY KEY (Image_ID, Tag),
        
        CONSTRAINT FK_Images FOREIGN KEY (Image_ID)
            REFERENCES Images(ID)
        
        )'''
    db_cursor.execute(drop_Tags)
    db_cursor.execute(drop_Images)
    db_cursor.execute(create_Images)
    db_cursor.execute(create_Tags)

def add_tag(id, tag):

    statement = '''
    INSERT INTO Tags
    VALUES (%s, %s)'''

    db_cursor.execute(statement, (id, tag))

    images_db.commit()

def search_by_tag(tag):
    statement = '''
    SELECT ID, Photographer, Tag
    FROM Tags
    INNER JOIN Images
    ON Tags.Image_ID = Images.ID
    WHERE Tag = %s
    '''

    db_cursor.execute(statement, (tag,))

    images_db.commit()

    result = db_cursor.fetchall()
    
    for photo in result:
        print(photo)

def search_by_id(photo_id):
    statement = '''
    SELECT ID, Photographer, Tag
    FROM Tags
    INNER JOIN Images
    ON Tags.Image_ID = Images.ID
    WHERE ID = %s;  '''

    db_cursor.execute(statement, (photo_id,))

    images_db.commit()

    result = db_cursor.fetchall()

    for photo_detail in result:
        print(photo_detail)

def drop_Tag(image_id):
    tag = input('''Which tag would you like to remove? -''')

    statement = '''
    SELECT * FROM Tags
    WHERE Image_ID = %s AND Tag = %s    '''

    db_cursor.execute(statement, (image_id, tag))

    images_db.commit()

    drop_Tag_statement = '''
    DELETE FROM Tags
    WHERE Image_ID = %s and Tag = %s'''

    db_cursor.execute(drop_Tag_statement, (image_id, tag))
    images_db.commit()

    updated = 'SELECT * FROM Tags WHERE Image_ID = %s'

    db_cursor.execute(updated, (image_id, ))

    result = db_cursor.fetchall()

    print('Tags for image ID', image_id, 'are now:')
    for photo in result:
        print(photo)





## TOP LEVEL STARTS HERE >_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>_>

images_db = mysql.connector.connect(
    host = 'localhost',
    user = 'root',
    passwd = '2914',
    database = 'PROJECT'
)

db_cursor = images_db.cursor(buffered=True, dictionary=True)

insert_string = '''INSERT INTO Images (Photo_Path, Photographer, URL_of_Photo, URL_of_Photographer, Date_Created)
VALUES (%s, %s, %s, %s, %s)'''

db_cursor.execute(insert_string, ('this is a photo path','Aastha Basu', 'www.photo/Aastha-Basu.com', 'www.photographer/Aastha-Basu.com', '2021-12-31'))

images_db.commit()

while True:
    choice = int(input('''
    Press 1 to recreate table and initialize data
    Press 2 to find images with a given tag
    Press 3 to find tags by image ID
    Press 4 to find images with multiple tags
    Press 5 to add a tag to an image
    Press 6 delete a tag from an image
    Press 7 to exit
    
    Enter desired choice: '''))         # 1, 2, 3, 4, 5, 6 DONE

    if choice == 1:        
        reCreate_tables()

        read_images()

    if choice == 2:
        search_tag = input('Enter tag: ')
        print('Searching for images...')

        search_by_tag(search_tag)

    if choice == 3:
        search_id = int(input('Enter image ID: '))
        print('Searching for image...')

        search_by_id(search_id)
    
    if choice == 4:
        ntags = int(input('Enter the number of tags you want to search for: '))
#        tags_tup = tuple()
        
        tags_search = '''SELECT * FROM Tags
        WHERE Tag in ("'''

        for i in range(ntags):
            tag = input('Enter tag: ')
            tags_search += tag
            tags_search += '", "'

            print(tags_search)
        
        tags_search += '")'
        
        print(tags_search)

        db_cursor.execute(tags_search, )

        result = db_cursor.fetchall()

        for i in result:
            print(i)

    if choice == 5:
        image_id = int(input('Enter image ID: '))
        tag = input('Enter tag to add to above image ID: ')

        add_tag(image_id, tag)

    if choice == 6:
        image_id = int(input('Enter image ID to view its tags: '))

        print('Image ID', image_id, 'has the following tags:')

        photos_with_id = '''SELECT * FROM Tags
        WHERE Image_ID = %s'''

        db_cursor.execute(photos_with_id, (image_id,))

        photos = db_cursor.fetchall()

        for photo in photos:
            print(photo)
        
        drop_Tag(image_id)

    if choice == 7:
        break