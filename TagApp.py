# import all the functionality of tkinter.
from tkinter import *
import json
import logging
import os
import mysql.connector
from pathlib import Path
from scrollImage import ScrollableImage





images_db = None
db_cursor = None


def main():

    # create a window as root using Tk() function
    root = Tk()
    root.geometry("400x600")
    # create a changecolor function
    # to change background color of window

    def changecolor(event):
        # get selected list box color item
        color = listbox.get(ANCHOR)
        # configure color to the window
        root.configure(bg=color)

    # create listbox
    listbox = Listbox(root, font=('times 20 bold'), height=5,
                      width=10, selectmode='single')
    # insert color items into listbox
    listbox.insert(1, 'pink')
    listbox.insert(2, 'skyblue')
    listbox.insert(3, 'green2')
    listbox.insert(4, 'red')
    listbox.pack(pady=30)
    # bind the click event with listbox and
    # call changecolor() function
    listbox.bind('<<ListboxSelect>>', changecolor)
    root.mainloop()



def drawMainWindow():

    logger.info("Begin  of drawMainWindow")

    # root: Create a root window, set the geometry to large

    # listsFrame: Create a lists Frame for the tags and images.

    # tagLabel
    # tagEntry (enter a tag to add)
    # tagAdd
    # remove (remove selected tag from list)
    # search (search for images with these tags)
    # tagList - listbox with scroll
    # imageLabel
    # imageList - listbox with iimages

    # imageDetailsFrame
    # path: label
    # name : entry, to change the name, default to path
    # tags: label with list of tags.
    # tagEntry: entry for specifying a label
    # addTagToImageBtn
    # removeTagFromImageBtn
    # Frame with Image


    ws = Tk()
    ws.title('PythonGuides')
    ws.geometry('100x200')


    img = PhotoImage(file='example.png')
    
    image_window = ScrollableImage (ws, image=img, scrollbarwidth=6, width=200, height=200)

    image_window.pack(fill='both', expand=1)
    ws.mainloop()

    logger.info("End of drawMainWindow")
    return


if __name__ == '__main__':
    logger = logging.getLogger('ImageApp')

    logging.basicConfig(
        filename='app.log',
        filemode='a',
       level=logging.DEBUG,
       format='%(asctime)s %(name)s %(message)s')

    logger.info('Invoked from main')

    images_db = mysql.connector.connect(
        host='localhost',
        user='aastha',
        password='Im4geT@g',
        database='imageTaggingProject'
    )

    db_cursor = images_db.cursor(buffered=True, dictionary=True)

    #main()
    drawMainWindow()

else:
    print( '=====================================')
    print ("Can not be run as module")
    




def isString(s): return True if (isinstance(s, str)) else False